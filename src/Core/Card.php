<?php

namespace App\Core;

/**
 * Class Card : Définition d'une carte à jouer
 * @package App\Core
 */
class Card
{
    private $name;
    private $color;

    public function __construct(string $name, string $color)
    {
        $this->name = $name;
        $this->color = $color;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getColor() : string
    {
        return $this->color;
    }

    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    public static function compare(Card $c1, Card $c2) : int
    {
        if ($c1->name === $c2->name && $c1->color === $c2->color) {
            return 0;
        }

        return ($c1->name > $c2->name) ? +1 : -1;
    }

    public function __toString()
    {
        return $this->name . ' de ' . $this->color;
    }
}