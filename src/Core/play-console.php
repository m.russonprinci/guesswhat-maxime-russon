<?php

require '../../vendor/autoload.php';

function promptUser($prompt)
{
    echo $prompt . "\n";
    return trim(readline("> "));
}

function getIntInput($prompt, $min, $max)
{
    do {
        $input = promptUser($prompt);
        if (!ctype_digit($input)) {
            echo "Veuillez entrer un nombre valide.\n";
        } elseif ($input < $min || $input > $max) {
            echo "Veuillez entrer un nombre entre $min et $max.\n";
        } else {
            return (int)$input;
        }
    } while (true);
}

function getYesNoInput($prompt)
{
    $input = strtolower(promptUser("$prompt (Oui/Non)"));
    if ($input === 'oui' || $input === 'o') {
        return true;
    } elseif ($input === 'non' || $input === 'n') {
        return false;
    } else {
        echo "Veuillez répondre par 'Oui' ou 'Non'.\n";
        return getYesNoInput($prompt);
    }
}

function chooseCardGame()
{
    echo "Choisissez la taille du jeu de cartes :\n";
    echo "1. Jeu de 32 cartes\n";
    echo "2. Jeu de 52 cartes\n";
    return getIntInput("Entrez votre choix :", 1, 2);
}

function chooseAttempts($maxAttempts)
{
    return getIntInput("Combien de tentatives voulez-vous ? (entre 1 et $maxAttempts) :", 1, $maxAttempts);
}

function wantHelp()
{
    return getYesNoInput("Souhaitez-vous de l'aide ?");
}

function playGame()
{
    echo " - PARAMÈTRAGE DE LA PARTIE - \n\n";

    $cardGameChoice = chooseCardGame();
    $cardGame = new App\Core\CardGame(
        $cardGameChoice === 1 ? App\Core\CardGame::factory32Cards() : App\Core\CardGame::factory52Cards()
    );

    $maxAttempts = $cardGame->countCards();
    $attemptChoice = chooseAttempts($maxAttempts);

    $withHelp = wantHelp();

    $secretCard = null;
    $game = new App\Core\Game($cardGame, $secretCard, $withHelp);

    echo "\n - LANCEMENT DE LA PARTIE - \n";
    echo "Jeu : " . $cardGame->countCards() . " cartes \n";
    echo "Nombre de tentatives : $attemptChoice \n";
    echo "Aide à la recherche : " . ($withHelp ? "Oui" : "Non") . "\n";

    $remainAttempt = $attemptChoice;

    // Gameplay
    $userCards = [];
    while ($remainAttempt > 0) {
        echo "\n\nVous avez $remainAttempt tentative(s).\n";

        $userCardName = promptUser("Entrez un nom de carte dans le jeu (exemples : Roi, 7, As) : ");
        // Valider et traiter le nom de la carte

        $userCardColor = promptUser("Entrez une couleur de carte dans le jeu (exemples : Coeur, Trefle, Carreau, Pique) : ");
        // Valider et traiter la couleur de la carte

        $remainAttempt--;

        $userCard = new \App\Core\Card($userCardName, $userCardColor);
        $userCards[] = $userCard;

        if ($game->isMatch($userCard)) {
            echo "Bravo ! Vous avez deviné la carte correctement.\n";
            break;
        } else {
            echo "Loupé ! Vous n'avez pas deviné la carte correctement.\n";
        }

        if ($game->getWithHelp()) {
            // Comparer la carte proposée avec la carte à deviner
            $resultCompare = $cardGame->compare($userCard, $game->getCardToGuess());

            // Afficher le résultat de la comparaison
            if ($resultCompare < 0) {
                echo "La carte proposée est trop basse.\n";
            } elseif ($resultCompare > 0) {
                echo "La carte proposée est trop haute.\n";
            }
        }
    }
}

do {
    playGame();

    echo "\nVoulez-vous recommencer ?\n";
    $restart = getYesNoInput("Voulez-vous recommencer ?");
} while ($restart);

echo "\n\n - FIN -";