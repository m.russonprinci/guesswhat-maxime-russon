<?php
namespace App\Core;

class Game
{
private $cardGame;
private $cardToGuess;
private $withHelp;

public function __construct(CardGame $cardGame = null, $cardToGuess = null, bool $withHelp = true)
{
if ($cardGame === null) {
$this->cardGame = new CardGame(CardGame::factory32Cards());
} else {
$this->cardGame = $cardGame;
}

if ($cardToGuess) {
$this->cardToGuess = $cardToGuess;
} else {
$this->cardGame->shuffle();
$this->cardToGuess = $this->cardGame->getCard(0);
}

$this->withHelp = $withHelp;
}

public function getWithHelp()
{
return $this->withHelp;
}

public function isMatch(Card $card)
{
return CardGame::compare($card, $this->cardToGuess) === 0;
}

public function getStatistics(): string
{
// TODO: Implementez cette méthode pour retourner des statistiques sur le jeu.
return "TODO : getStatistics()";
}

public function getCardToGuess(): Card
{
return $this->cardToGuess;
}
}