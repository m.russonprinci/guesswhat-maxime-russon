<?php

namespace App\Core;

use phpDocumentor\Reflection\Types\Integer;

class CardGame
{
    const ORDER_COLORS=['Trefle'=> 1, 'Carreau'=>2, 'Coeur'=>3, 'Pique'=>4 ];

    private $cards;

    public function __construct(array $cards)
    {
        $this->cards = $cards;
    }

    public function shuffle()
    {
        shuffle($this->cards);
    }

    public static function compare(Card $c1, Card $c2) : int
    {
        $c1Name = strtolower($c1->getName());
        $c2Name = strtolower($c2->getName());

        if ($c1Name === $c2Name) {
            return 0;
        }

        return ($c1Name > $c2Name) ? +1 : -1;
    }

    public static function factory32Cards() : array {
        $cards = [];
        $colors = array_keys(self::ORDER_COLORS);
        $names = ['As', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];

        foreach ($colors as $color) {
            foreach ($names as $name) {
                $cards[] = new Card($name, $color);
            }
        }

        return $cards;
    }

    public function getCard(int $index) : Card {
        return  $this->cards[$index];
    }

    public function sort() {
        usort($this->cards, ['CardGame', 'compare']);
    }

    public function __toString()
    {
        return 'CardGame : '.count($this->cards).' carte(s)';
    }
}