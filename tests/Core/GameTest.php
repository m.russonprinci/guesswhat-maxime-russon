<?php

namespace App\Tests\Core;

use App\Core\CardGame;
use App\Core\Game;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
  public function testDefaultValues() {
    $jeuDeCartes = new CardGame(CardGame::factory32Cards());
    $game = new Game($jeuDeCartes);
    $this->assertNotNull($game->getCardToGuess());
    $this->assertTrue($game->getWithHelp());
  }
    public function testIsMatch()
    {
        $cardGame = $this->createMock(CardGame::class);
        $cardToGuess = $this->createMock(Card::class);
        $game = new Game($cardGame, $cardToGuess);

        $card = $this->createMock(Card::class);
        $cardGame->method('compare')
            ->willReturn(0);

        $this->assertTrue($game->isMatch($card));
    }

    public function testGetStatistics()
    {
        $cardGame = $this->createMock(CardGame::class);
        $cardToGuess = $this->createMock(Card::class);
        $game = new Game($cardGame, $cardToGuess);

        $this->assertEquals("TODO : gestStatistics()", $game->getStatistics());
    }

    public function testGetCardToGuess()
    {
        $cardGame = $this->createMock(CardGame::class);
        $cardToGuess = $this->createMock(Card::class);
        $game = new Game($cardGame, $cardToGuess);

        $this->assertEquals($cardToGuess, $game->getCardToGuess());
    }
}

