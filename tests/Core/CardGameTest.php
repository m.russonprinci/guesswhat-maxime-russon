<?php

namespace App\Tests\Core;

use App\Core\Card;
use App\Core\CardGame;
use PHPUnit\Framework\TestCase;

class CardGameTest extends TestCase
{

  public function testToString2Cards()
  {
    $jeudecartes = new CardGame([new Card('As', 'Pique'), new Card('Roi', 'Coeur')]);
    $this->assertEquals('CardGame : 2 carte(s)',$jeudecartes->__toString());
  }

  public function testToString1Card()
  {
    $cardGame = new CardGame([new Card('As', 'Pique')]);
    $this->assertEquals('CardGame : 1 carte(s)',$cardGame->__toString());
  }

    public function testCompare()
    {
        $cardGame = new CardGame([new Card('As', 'Pique'), new Card('Roi', 'Coeur')]);
        $this->assertEquals(-1, $cardGame->compare(0, 1));
        $this->assertEquals(1, $cardGame->compare(1, 0));
        $this->assertEquals(0, $cardGame->compare(0, 0));
    }

    public function testShuffle()
    {
        $cardGame = new CardGame([new Card('As', 'Pique'), new Card('Roi', 'Coeur')]);
        $cardGameBeforeShuffle = clone $cardGame;
        $cardGame->shuffle();
        $this->assertNotEquals($cardGameBeforeShuffle, $cardGame);
    }

    public function testGetCard()
    {
        $cardGame = new CardGame([new Card('As', 'Pique'), new Card('Roi', 'Coeur')]);
        $this->assertInstanceOf(Card::class, $cardGame->getCard(0));
        $this->assertInstanceOf(Card::class, $cardGame->getCard(1));
    }

    public function testFactoryCardGame32()
    {
        $cardGame32 = CardGame::factoryCardGame32();
        $this->assertInstanceOf(CardGame::class, $cardGame32);
        $this->assertCount(32, $cardGame32->getCards());
    }
}