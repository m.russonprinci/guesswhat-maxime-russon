<?php

namespace App\Tests\Core;

use PHPUnit\Framework\TestCase;
use App\Core\Card;

class CardTest extends TestCase
{

    public function testName()
    {
        $card = new Card('As', 'Trefle');
        $this->assertEquals('As', $card->getName());

        $card = new Card('2', 'Trefle');
        $this->assertEquals('2', $card->getName());
    }

    public function testColor()
    {
        $card = new Card('As', 'Trefle');
        $this->assertEquals('Trefle', $card->getColor());

        $card = new Card('As', 'Pique');
        $this->assertEquals('Pique', $card->getColor());
    }


    public function testCompareSameCard()
  {
    $card1 = new Card('As', 'Trefle');
    $card2 = new Card('As', 'Trefle');
    $this->assertEquals(0, CardGame::compare($card1,$card2));
  }





    public function testCompareSameNameNoSameColor()
    {
        $card1 = new Card('As', 'Trefle');
        $card2 = new Card('As', 'Coeur');

        // Supposons que la couleur 'Trefle' est inférieure à 'Coeur'
        $this->assertTrue(Card::compare($card1, $card2) < 0);
    }

    public function testCompareNoSameNameSameColor()
    {
        $card1 = new Card('As', 'Trefle');
        $card2 = new Card('Roi', 'Trefle');

        // Supposons que 'As' est supérieur à 'Roi'
        $this->assertTrue(Card::compare($card1, $card2) > 0);
    }

    public function testCompareNoSameNameNoSameColor()
    {
        $card1 = new Card('As', 'Trefle');
        $card2 = new Card('Roi', 'Coeur');

        // Supposons que 'As' est supérieur à 'Roi' et 'Trefle' est inférieur à 'Coeur'
        // Donc, la comparaison dépend de la logique de votre jeu
        // Ici, nous supposons que le nom de la carte est plus important que la couleur
        $this->assertTrue(Card::compare($card1, $card2) > 0);
    }

    public function testToString()
    {
        $card = new Card('As', 'Trefle');
        $this->assertEquals('As de Trefle', $card->__toString());
    }
}